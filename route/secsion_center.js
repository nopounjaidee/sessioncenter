const express = require('express');
const router = express.Router();
const sql = require("../db");

router.post('/',(req, res) => {
    const user = req.body.user;
    const project_type = req.body.pjtype;
    var usersplice = user.slice(0, 3);
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, "0");
    var mm = String(today.getMonth() + 1).padStart(2, "0");
    var yyyy = today.getFullYear();
    var hour = today.getHours();
    var times = today.getMinutes() + 5 ;
    var sec = today.getSeconds() ;
    var expirydate = yyyy + "/" + mm + "/" + dd + " " +hour+":"+times+":"+sec;
    const session = gencode(8)+"-"+usersplice+gencode(3)+"-"+gencode(8)
    

    let check_ses = "INSERT INTO session (username,session,status,create_at,expire_at)"
    + "VALUES ('" +user + "','" + session+ "',0,NOW(),'" + expirydate+ "');";
    sql.query(check_ses, (err, result) => {
        if (err) {
            return res.status(500).send(err);
        }else{
            res.json({message:"session Can use up to 5 minutes",status:"sucsess",session:session});
        }
    });
});


function gencode(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}
module.exports = router;
