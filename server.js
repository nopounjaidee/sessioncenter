const express = require("express");
const bodyparser = require("body-parser");
const app = express();
require('./db')
app.use(bodyparser.json());
app.use(bodyparser.urlencoded());
app.use(function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    //res.setHeader('Access-Control-Allow-Origin','http://localhost:4200');
    res.setHeader('Access-Control-Allow-Methods','GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers','content-type, x-access-token');
    res.setHeader('Access-Control-Allow-Credentials','*');
    next();
});
const example = require('./route/secsion_center');
app.use('/secsioncenter',example);
app.get('*',(req, res) => {
    res.send("API ไม่ถูกต้อง")
});

app.listen(3003,() => {
    console.log("server Is Running......port 3003");
});